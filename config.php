<?php

function getConfig() {
  return array(
    'send_to'   => 'remigijui@gmail.com',
    'from'      => 'info@gardencube.lt',
    'from_name' => 'GardenCube',
    'file_name' => 'namukas.pdf',
  );
}

function getProvider() {
  return array(
    'name'     => 'UAB „Pavadinimas“',
    'code'     => '12345679',
    'vat'      => 'LT-123456789',
    'address1' => 'Pavadinimas g. 25-35',
    'address2' => 'LT-12345 Miestas, Lithuania',
    'phone'    => '+370 41 123456',
    'mobile'   => '+370 123 12345',
    'email'    => 'info@imonespavadinimas.lt',
    'website'  => 'www.imonespavadinimas.lt',
  );
}

?>