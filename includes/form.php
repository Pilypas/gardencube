<td class="email_form" width="250px">
  <div class="form_title">Jei norite gauti kainos pasiūlymą, užpildykite šią formą ir paspauskite "Užsakyti" apačioje.</div><br/>
  <table cellspacing="3" cellpadding="3" align="center">
    <tr>
      <td>
        <span id="form-titles">Vardas</span><br/>
        <input style="width:100%;" type="text" id="name" name="name" value="" size="34">
      </td>
    </tr>
    <tr>
      <td>
         <span id="form-titles">Pavardė</span><br/>
        <input style="width:100%;" type="text" id="surname" name="surname" value="" size="34">
      </td>
    </tr>
    <tr>
      <td>
         <span id="form-titles">El. pašto adresas</span><br/>
        <input style="width:100%;" type="email" id="email" name="email" value="" size="34">
      </td>
    </tr>
    <tr>
      <td>
         <span id="form-titles">Telefono Nr.</span><br/>
        <input style="width:100%;" type="text" id="phone" name="phone" value="" size="34">
      </td>
    </tr>
    <tr>
      <td>
         <span id="form-titles">Adresas</span><br/>
        <input style="width:100%;" type="text" id="address" name="address" value="" size="34">
      </td>
    </tr>
    <tr>
      <td>
         <span id="form-titles">Pastabos</span><br/>
        <textarea name="note" id="note" rows="5" cols="26"></textarea>
      </td>
    </tr>
  </table>
  <div class="button_box">
    <a href="#" class="form_btn" onclick="submitForm('preview')">Formuoti PDF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br/><br/>
    <a href="" class="form_btn">Išvalyti duomenis</a>
  </div>
</td>
