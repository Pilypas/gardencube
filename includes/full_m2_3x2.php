<td>
  <div id="work-container" data-parts="[[[2,3]], [[4,5,6]]]">
    <div id="base-image-face" class="base-image">
      <img src="images/models/m2_3x2_face.png" width="607" height="455" />
    </div>

    <div id="base-image-back" class="base-image">
      <img src="images/models/m2_3x2_back.png" width="607" height="455" />
    </div>

    <div id="marker-one"></div>
    <div id="marker-two"></div>

    <div class="side-face">
      <label data-slot="2" class="slot circle-2-m2">1</label>
      <label data-slot="3" class="slot circle-3-m2">2</label>
      <label data-slot="4" class="slot circle-4-m2">3</label>
      <label data-slot="5" class="slot circle-5-m2">4</label>
      <label data-slot="6" class="slot circle-6-m2">5</label>

      <?php include('layers_full_face.php') ?>
    </div>

    <div class="side-back">
      <label data-slot="2" class="slot circle-2-m2">6</label>
      <label data-slot="3" class="slot circle-3-m2">7</label>
      <label data-slot="4" class="slot circle-4-m2">8</label>
      <label data-slot="5" class="slot circle-5-m2">9</label>
      <label data-slot="6" class="slot circle-6-m2">10</label>

      <?php include('layers_full_back.php') ?>
    </div>
  </div>

  <div id="side-select-face" class="side" data-side="face">
    <label>
      A, B pusės
      <input type="radio" name="pus" value="p1" />
      <img src="images/models/m2_3x2_face.png" align="center" border="1" width="150" height="100"/>
    </label>
  </div>

  <div id="side-select-back" class="side" data-side="back">
    <label>
      <input type="radio" name="pus" value="p2" />
      <img src="images/models/m2_3x2_back.png" align="center" border="1" width="150" height="100"/>
      C, D pusės
    </label>
  </div>
</td>

<?php include('wall_selector_3.php') ?>
