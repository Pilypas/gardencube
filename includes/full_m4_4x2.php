<td>
  <div id="work-container" data-parts="[[[2,3]], [[4,5],[7,8]]]">
    <div id="base-image-face" class="base-image">
      <img src="images/models/m4_4x2_face.png" width="607" height="455" />
    </div>

    <div id="base-image-back" class="base-image">
      <img src="images/models/m4_4x2_back.png" width="607" height="455" />
    </div>

    <div id="marker-one"></div>
    <div id="marker-two"></div>

    <div class="side-face">
      <label data-slot="2" class="slot circle-2-m4">1</label>
      <label data-slot="3" class="slot circle-3-m4">2</label>
      <label data-slot="4" class="slot circle-4-m4">3</label>
      <label data-slot="5" class="slot circle-5-m4">4</label>
      <label data-slot="7" class="slot circle-6-m4">5</label>
      <label data-slot="8" class="slot circle-7-m4">6</label>

      <?php include('layers_full_face.php') ?>
    </div>

    <div class="side-back">
      <label data-slot="2" class="slot circle-2-m4">7</label>
      <label data-slot="3" class="slot circle-3-m4">8</label>
      <label data-slot="4" class="slot circle-4-m4">9</label>
      <label data-slot="5" class="slot circle-5-m4">10</label>
      <label data-slot="7" class="slot circle-6-m4">11</label>
      <label data-slot="8" class="slot circle-7-m4">12</label>

      <?php include('layers_full_back.php') ?>
    </div>
  </div>

  <div id="side-select-face" class="side" data-side="face">
    <label>
      A, B pusės
      <input type="radio" name="pus" value="p1" />
      <img src="images/models/m4_4x2_face.png" align="center" border="1" width="150" height="100"/>
    </label>
  </div>

  <div id="side-select-back" class="side" data-side="back">
    <label>
      <input type="radio" name="pus" value="p2" />
      <img src="images/models/m4_4x2_back.png" align="center" border="1" width="150" height="100"/>
      C, D pusės
    </label>
  </div>
</td>

<?php include('wall_selector_2.php') ?>
