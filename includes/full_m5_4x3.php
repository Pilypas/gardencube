<td>
  <div id="work-container" data-parts="[[[1,2,3]], [[4,5,7,8]]]">
    <div id="base-image-face" class="base-image">
      <img src="images/models/m5_4x3_face.png" width="607" height="455" />
    </div>

    <div id="base-image-back" class="base-image">
      <img src="images/models/m5_4x3_back.png" width="607" height="455" />
    </div>

    <div id="marker-one"></div>
    <div id="marker-two"></div>

    <div class="side-face">
      <label data-slot="1" class="slot circle-1-m5">1</label>
      <label data-slot="2" class="slot circle-2-m5">2</label>
      <label data-slot="3" class="slot circle-3-m5">3</label>
      <label data-slot="4" class="slot circle-4-m5">4</label>
      <label data-slot="5" class="slot circle-5-m5">5</label>
      <label data-slot="7" class="slot circle-6-m5">6</label>
      <label data-slot="8" class="slot circle-7-m5">7</label>

      <?php include('layers_full_face.php') ?>
    </div>

    <div class="side-back">
      <label data-slot="1" class="slot circle-1-m5">8</label>
      <label data-slot="2" class="slot circle-2-m5">9</label>
      <label data-slot="3" class="slot circle-3-m5">10</label>
      <label data-slot="4" class="slot circle-4-m5">11</label>
      <label data-slot="5" class="slot circle-5-m5">12</label>
      <label data-slot="7" class="slot circle-6-m5">13</label>
      <label data-slot="8" class="slot circle-7-m5">14</label>

      <?php include('layers_full_back.php') ?>
    </div>
  </div>

  <div id="side-select-face" class="side" data-side="face">
    <label>
      A, B pusės
      <input type="radio" name="pus" value="p1" />
      <img src="images/models/m5_4x3_face.png" align="center" border="1" width="150" height="100"/>
    </label>
  </div>

  <div id="side-select-back" class="side" data-side="back">
    <label>
      <input type="radio" name="pus" value="p2" />
      <img src="images/models/m5_4x3_back.png" align="center" border="1" width="150" height="100"/>
      C, D pusės
    </label>
  </div>
</td>

<?php include('wall_selector_3.php') ?>
