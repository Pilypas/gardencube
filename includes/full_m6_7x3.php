<td>
  <div id="work-container" data-parts="[[[1,2,3]], [[4,5,7,8],[16,17,18]]]">
    <div id="base-image-face" class="base-image">
      <img src="images/models/m6_7x3_face.png" width="607" height="455" />
    </div>

    <div id="base-image-back" class="base-image">
      <img src="images/models/m6_7x3_back.png" width="607" height="455" />
    </div>

    <div id="marker-one"></div>
    <div id="marker-two"></div>

    <div class="side-face">
      <label data-slot="9" class="slot circle-8">1</label>
      <label data-slot="10" class="slot circle-9">2</label>
      <label data-slot="11" class="slot circle-10">3</label>
      <label data-slot="12" class="slot circle-11">4</label>
      <label data-slot="13" class="slot circle-12">5</label>
      <label data-slot="14" class="slot circle-13">6</label>
      <label data-slot="15" class="slot circle-14">7</label>
      <label data-slot="16" class="slot circle-15">8</label>
      <label data-slot="17" class="slot circle-16">9</label>
      <label data-slot="18" class="slot circle-17">10</label>

      <?php include('layers_full_face.php') ?>
    </div>

    <div class="side-back">
      <label data-slot="9" class="slot circle-8">11</label>
      <label data-slot="10" class="slot circle-9">12</label>
      <label data-slot="11" class="slot circle-10">13</label>
      <label data-slot="12" class="slot circle-11">14</label>
      <label data-slot="13" class="slot circle-12">15</label>
      <label data-slot="14" class="slot circle-13">16</label>
      <label data-slot="15" class="slot circle-14">17</label>
      <label data-slot="16" class="slot circle-15">18</label>
      <label data-slot="17" class="slot circle-16">19</label>
      <label data-slot="18" class="slot circle-17">20</label>

      <?php include('layers_full_back.php') ?>
    </div>
  </div>

  <div id="side-select-face" class="side" data-side="face">
    <label>
      A, B pusės
      <input type="radio" name="pus" value="p1" />
      <img src="images/models/m6_7x3_face.png" align="center" border="1" width="150" height="100"/>
    </label>
  </div>

  <div id="side-select-back" class="side" data-side="back">
    <label>
      <input type="radio" name="pus" value="p2" />
      <img src="images/models/m6_7x3_back.png" align="center" border="1" width="150" height="100"/>
      C, D pusės
    </label>
  </div>
</td>

<?php include('wall_selector_3.php') ?>
