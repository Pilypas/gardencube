<td style="padding-top: 44px; vertical-align: top; width: 35%;">
  <div class="generated-image" id="combo_one">
    <img src="images/models/m6_7x3_face.png" width="340" height="260">

    <?php include('layers_mini_face.php') ?>
  </div>

  <div class="generated-content">
    <span class="generated-content-header">Pavėsinės A pusės sienos</span><br/>
    <span id="desc_a1"></span><br/>
    <span id="desc_a2"></span><br/>
    <span id="desc_a3"></span><br/><br/>
    <span class="generated-content-header">Pavėsinės B pusės sienos</span><br/>
    <span id="desc_b1"></span><br/>
    <span id="desc_b2"></span><br/>
    <span id="desc_b3"></span><br/>
    <span id="desc_b4"></span><br/>
    <span id="desc_b5"></span><br/>
    <span id="desc_b6"></span><br/>
    <span id="desc_b7"></span><br/><br/>
    <span class="generated-content-header">Lietvamzdžiai: </span>
    <span id="desc_gutters"></span>
  </div>
</td>

<td style="padding-top: 44px; vertical-align: top; width: 35%;">
  <div class="generated-image" id="combo_two">
    <img src="images/models/m6_7x3_back.png" width="340" height="260">

    <?php include('layers_mini_back.php') ?>
  </div>

  <div class="generated-content">
    <span class="generated-content-header">Pavėsinės C pusės sienos</span><br/>
    <span id="desc_c1"></span><br/>
    <span id="desc_c2"></span><br/>
    <span id="desc_c3"></span><br/><br/>
    <span class="generated-content-header">Pavėsinės D pusės sienos</span><br/>
    <span id="desc_d1"></span><br/>
    <span id="desc_d2"></span><br/>
    <span id="desc_d3"></span><br/>
    <span id="desc_d4"></span><br/>
    <span id="desc_d5"></span><br/>
    <span id="desc_d6"></span><br/>
    <span id="desc_d7"></span><br/>
  </div>
</td>

<?php include('form.php') ?>
