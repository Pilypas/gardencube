<div id="wall-types-header">Pasirinkite sienų tipą</div>

<td>
  <label id="wall-0" class="wall" data-label="">
    <div class="wall-selector">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tuščia<br><span id="to-right-img"><img src="images/walls/w0_tuscia.png" height="100"></span>
      <input type="radio" name="tipas" id="tipas" value="t1"  />
    </div>
  </label>
  <label id="wall-2" class="wall" data-label="Medis">
    <div class="wall-selector">
      &nbsp;&nbsp;Medinės lentelės<br><span id="to-right-img"><img src="images/walls/w2_medines_lenteles.png" height="100"></span>
      <input type="radio" name="tipas" id="tipas" value="t1"  />
    </div>
  </label>
  <label id="wall-4" class="wall" data-label="Aliumininis langas">
    <div class="wall-selector">
      &nbsp;&nbsp;Aliumininiai langai<br><span id="to-right-img"><img src="images/walls/w4_aliumininiai_langai.png" height="100"></span>
      <input type="radio" name="tipas" id="tipas" value="t1"  />
    </div>
  </label>
  <label id="wall-6" class="wall" data-label="Medinis langas">
    <div class="wall-selector">
      &nbsp;&nbsp;Mediniai langai<br><span id="to-right-img"><img src="images/walls/w6_mediniai_langai.png" height="100"></span>
      <input type="radio" name="tipas" id="tipas" value="t1"  />
    </div>
  </label>
</td>
<td>
  <label id="wall-1" class="wall" data-label="Durys">
    <div class="wall-selector">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Durys<br><span id="to-right-img"><img src="images/walls/w1_durys.png" width="45" height="100"></span>
      <input type="radio" name="tipas" id="tipas" value="t1"  />
    </div>
  </label>
  <label id="wall-3" class="wall" data-label="Žaliuzės">
    <div class="wall-selector">
      &nbsp;&nbsp;Dekoratyvinė sienelė<br><span id="to-right-img"><img src="images/walls/w3_dekoratyvine_sienele.png" height="100"></span>
      <input type="radio" name="tipas" id="tipas" value="t1"  />
    </div>
  </label>
  <label id="wall-5" class="wall" data-label="Sienelė su stiklu">
    <div class="wall-selector">
      &nbsp;&nbsp;Sienelė su stiklu<br><span id="to-right-img"><img src="images/walls/w5_sienele_su_stiklu.png" height="100"></span>
      <input type="radio" name="tipas" id="tipas" value="t1"  />
    </div>
  </label>
  <label id="wall-7" class="wall" data-label="Slankiojanti sistema">
    <div class="wall-selector">
      &nbsp;&nbsp;Stumdoma sistema<br><span id="to-right-img"><img src="images/walls/w7_stumdoma_sistema_2.png" height="100" width="60"></span>
      <input type="radio" name="tipas" id="tipas" value="t1"  />
    </div>
  </label>
</td>
