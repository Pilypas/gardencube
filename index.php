﻿<?php
  include_once('config.php');

  $provider = getProvider();
  $date = date('Y m d');
  $oid  = date('His'); // <<< TEMPORARY
  $ordernr = date('Ymd').$oid;

?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Pavėsinės - Susikonstruokite pavėsinę pagal savo poreikius!</title>

  <link href="styles/style.css" rel="stylesheet" type="text/css"/>
  <link href="styles/layers.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
  <link rel="shortcut icon" href="images/tema/G.ico"/>

  <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script type="text/javascript" src="js/jquery.smartWizard-2.0.js"></script>
  <script type="text/javascript" src="js/html2canvas.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
  <script type="text/javascript">
    var _T = {
      "Previous": "Atgal", "Next": "Toliau", "Finish": "Užsakyti",
      "Build one more": "Susikonstruokite dar vieną",
      "Please select a model": "Prašome pasirinkti namelio modelį!",
      "Please enter e-mail address": "Prašome įvesti el.pašto adresą",
      "E-mail address is invalid!": "El.pašto adresas neteisingas!"
    };
  </script>
</head>
<body>
  <div id="dialog" title="Ačiū!" style="display:none">
    <p>Dėkojame, kad naudojatės mūsų namelių modeliavimo sistema.</p>
    <p><b>Jūsų užsakymas gautas</b> - susisieksime su Jumis artimiausiu metu.</p>
  </div>

  <div class="header">
    <a href="/" title="Pradžia"><img src="images/tema/logo.jpg" border="0" /></a>
    <h3 id="subheader">Susikonstruokite&nbsp;pavėsinę&nbsp;pagal&nbsp;savo&nbsp;poreikius!</h3>
  </div>

  <table align="center" border="0" cellpadding="0" cellspacing="0">
    <tr><td>

      <form id="wizard-form" action="maker.php" method="POST">
        <input type="hidden" name="preview" id="preview" value="" />
        <input type="hidden" name="image_one" id="image_one" />
        <input type="hidden" name="image_two" id="image_two" />
        <input type="hidden" name="desc_side_a" id="desc_side_a" />
        <input type="hidden" name="desc_side_b" id="desc_side_b" />
        <input type="hidden" name="desc_side_c" id="desc_side_c" />
        <input type="hidden" name="desc_side_d" id="desc_side_d" />
        <input type="hidden" name="desc_gutter" id="desc_gutter" />
		<input type="hidden" name="desc_model_name" id="desc_model_name" />

        <!-- tabs -->
        <div id="wizard" class="swMain">
          <ul>
            <li>
              <a href="#step-1">
                <span class="stepNumber">1</span>
                <span class="stepDesc">
                   Pasirinkite modelį<br />
                   <small>Galimi 6 namelių modeliai</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#step-2">
                <span class="stepNumber">2</span>
                <span class="stepDesc">
                  Sienų tipas<br />
                  <small>Nurodykite sienų tipą</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#step-3">
                <span class="stepNumber">3</span>
                <span class="stepDesc">
                  Lietvamzdžiai<br />
                  <small>Pasirinkite lietvamzdžius</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#step-4">
                <span class="stepNumber">4</span>
                <span class="stepDesc">
                  Užklausos forma<br />
                  <small>Užpildykite užklausos formą</small>
                </span>
              </a>
            </li>
          </ul>

          <div id="step-1">
            <h2 class="step-title"><span id="to-right">Namelio modelio pasirinkimas</span></h2>

            <table cellspacing="10" cellpadding="10" id="model-table">
              <tbody>
                <tr>
                  <td>
                    <label id="model-name-title">1. Gaminio pavadinimas M1<img class="models" src="images/models/m1_preview.jpg">
                    <input id="model-name-title-input" type="radio"  name="model" value="m1_2x2" data-title="M1" /></label>
                  </td>
                  <td>
                    <label id="model-name-title">2. Gaminio pavadinimas M2<img class="models" src="images/models/m2_preview.jpg">
                    <input id="model-name-title-input" type="radio" name="model" value="m2_3x2" data-title="M2" /></label>
                  </td>
                  <td>
                    <label id="model-name-title">3. Gaminio pavadinimas M3<img class="models" src="images/models/m3_preview.jpg">
                    <input id="model-name-title-input" type="radio" name="model" value="m3_3x3" data-title="M3" /></label>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label id="model-name-title">4. Gaminio pavadinimas M4<img class="models" src="images/models/m4_preview.jpg">
                    <input id="model-name-title-input" type="radio" name="model" value="m4_4x2" data-title="M4" /></label>
                  </td>
                  <td>
                    <label id="model-name-title">5. Gaminio pavadinimas M5<img class="models" src="images/models/m5_preview.jpg">
                    <input id="model-name-title-input" type="radio" name="model" value="m5_4x3" data-title="M5" /></label>
                  </td>
                  <td>
                    <label id="model-name-title">6. Gaminio pavadinimas M6<img class="models" src="images/models/m6_preview.jpg">
                    <input id="model-name-title-input" type="radio" name="model" value="m6_7x3" data-title="M6" /></label>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div id="step-2">
            <h2 class="step-title"><span id="to-right">Gaminio pavadinimas <span id="model_name"></span></span></h2>
            <h3 class="step-title-small"><span id="to-right">Pasirinkite sekciją, kuriai priskirsite jūsų pasirinktą sienų tipą</span></h3>

			
            <div id="model-builder" class="">
              <div id="wall-types-header">Pasirinkite sienų tipą</div>
              <table cellspacing="3" cellpadding="3" >
                <tr></tr>
              </table>
            </div>
		  </div>

          <div id="step-3">
            <h2 class="step-title"><span id="to-right">Lietvamzdžių pasirinkimas</span></h2>
            <table cellspacing="3" cellpadding="3" align="center" style="margin-top: 130px;">
              <tr>
                <td align="center" colspan="3">
                  <div class="gutters_off">
                    <img src="images/gutters_off.png" width="385" height="360" />
                  </div>
                  <div class="gutters_on">
                    <img src="images/gutters_on.png" width="385" height="360" />
                  </div>
                </td>
              </tr>
              <tr><br/>
                <td style="text-align: center;">
                  <label id="to-right-gutters"><input type="radio" name="gutters" class="gutters" value="no" data-label="nereikalingi" checked="checked" />Nereikalingi</label>
                  <label><input type="radio" name="gutters" class="gutters" value="yes" data-label="reikalingi" />Reikalingi</label>
                </td>
              </tr>
            </table>
          </div>

          <div id="step-4">
            <h2 class="step-title"><span id="to-right">Gaminio pavadinimas <span id="step4_model_name"></span> apžvalga</span></h2>
            <h3 class="step-title-small"><span id="to-right">Užklausos Nr. <?= $ordernr ?> Data <?= $date ?></span></h3>
            <table id="model-result" class="" cellspacing="3" cellpadding="3" align="center" style="margin:0 5px 0 5px;">
              <tr></tr>
            </table>
          </div>

        </div>
      </form>

    </td></tr>
  </table>

  <div id="footer">
    <div class="footer_an1">
      <?= $provider['name'] ?><br>
      Įm. k. <?= $provider['code'] ?><br>
      PVM k. <?= $provider['vat'] ?>
    </div>
    <div class="footer_an2">
      <?= $provider['address1'] ?></br>
      <?= $provider['address2'] ?>
    </div>
    <div class="footer_an3">
      Tel. <?= $provider['phone'] ?></br>
      Mob. tel. <?= $provider['mobile'] ?>
    </div>
    <div class="footer_an4">
      <a href="mailto:<?= $provider['email'] ?>">El. p. <?= $provider['email'] ?></a><br/>
      <a href="http://<?= $provider['website'] ?>"target="_blank"><?= $provider['website'] ?></a>
    </div>
    <div class="footer_an5">
      <a href="http://fb.com"target="_blank"><img src="images/tema/fb.png" border="0"/></a>
      <a href="http://twitter.com" target="_blank"><img src="images/tema/twitter.png" border="0"/></a>
      <a href="http://youtu.be" target="_blank"><img src="images/tema/yt.png" border="0"/></a>
      <br/>
      <a href="http://vimeo.com" target="_blank"><img src="images/tema/vimeo.png" border="0"/></a>
      <a href="http://plus.google.com" target="_blank"><img src="images/tema/g+.png" border="0"/></a>
    </div>
  </div>
</body>
</html>
