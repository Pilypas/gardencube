// BEGIN module

  // map slot to markers ids (.marker-N)
  var slot_markers = {
    '1': [1, 2],
    '2': [2, 3],
    '3': [3, 4],
    '4': [5, 6],
    '5': [6, 7],
    '6': [7, 8],    // m2,m3
    '7': [9, 10],   // m4,m5
    '8': [10, 11],  // m4,m5
    '9':  [12, 13], // m6..(1)
    '10': [13, 14], // m6..(2)
    '11': [14, 15], // m6..(3)
    '12': [16, 17], // m6..(4)
    '13': [17, 18], // m6..(5)
    '14': [19, 20], // m6..(7)
    '15': [20, 21], // m6..(8)
    '16': [22, 23],
    '17': [23, 24],
    '18': [24, 25]
  };

  // map some slots on m6 to lower-numbered ones
  // this allows to avoid some level duplication
  var slot_normal = {
    '9':  1,
    '10': 2,
    '11': 3,
    '12': 4,
    '13': 5,
    '14': 7,
    '15': 8
  };

  var current_side, current_slot, assignment;

  // --- event bindings ---

  $(document).ready(function(){
    $('#wizard').smartWizard({
      transitionEffect:   'slideleft',
      onLeaveStep:        onLeaveStep,
      onShowStep:         onShowStep,
      onFinish:           onFinish,
      enableFinishButton: true,
      keyNavigation:      false,
      labelPrevious:      T('Previous'),
      labelNext:          T('Next'),
      labelFinish:        T('Finish')
    });
    if (location.hash == '#done') showDoneDialog();

    $('#model-table').on('click', 'td', onModelClick);
    $('#model-builder').on('click', '.side', onSideClick);
    $('#model-builder').on('click', '.slot', onSlotClick);
    $('#model-builder').on('click', '.wall', onWallClick);
    $('.gutters').on('click', onGuttersClick);
  });

  function showDoneDialog() {
    var buttons = {};
    buttons[T('Build one more')] = function(){ $(this).dialog('close') };
    $('#dialog').dialog({
      resizable: false,
      modal:     true,
      buttons:   buttons
    });
  }

  function onModelClick() {
    var $el = $(this);
    $el.closest('table').find('.active').removeClass('active');
    $el.addClass('active');
    $el.find('input:radio').attr('checked', 'checked');
  }

  function onSideClick() {
    $('.side').removeClass('active');
    var $el = $(this);
    $el.addClass('active');
    current_side = $el.data('side');
    var another_side = current_side == 'face' ? 'back' : 'face';
    $('#base-image-'+another_side).hide();
    $('#base-image-'+current_side).show();
    $('.side-'+another_side).hide();
    $('.side-'+current_side).show();
    $('.side-'+current_side+' .slot:first').click();
  }

  function onSlotClick() {
    var $el = $(this);
    $el.closest('div').find('.active').removeClass('active');
    $el.addClass('active');
    var slot = $el.data('slot');
    setMarkers(slot);
    current_slot = normalizeSlot(slot);
  }

  function onWallClick() {
    var $el = $(this);
    $('.wall-selector').removeClass('active');
    $el.find('.wall-selector').addClass('active');
    var wall = parseInt($el.attr('id').substr(5));
    setLayers(wall);
  }

  function onGuttersClick() {
    var on = $(this).val() == 'yes';
    $('.gutters_off').toggle(!on);
    $('.gutters_on').toggle(on);
  }

  function setMarkers(slot) {
    var markers = slot_markers[slot];
    $('#marker-one').removeClass().addClass('marker-'+markers[0]);
    $('#marker-two').removeClass().addClass('marker-'+markers[1]);
  }

  // --- layer management ---

  function setLayers(wall) {
    // WARNING: preventing event coming twice!
    var isset = assignment[current_side+current_slot];
    if (isset && isset.wall == wall) return; // slightly wrong
    
    var got = findPosition(current_slot, wall);
    if (!got) return;
    var size = wallSize(wall);
    for (var i=0; i<size; i++) {
      removeAssignment(current_side, got, i);
    }
    createAssignment(current_side, got, wall);
  }

  function findPosition(slot, wall) {
    var found = findBlock(slot);
    var size = wallSize(wall);
    if (!found || found.block.length < size) return undefined;
    if (found.index + size <= found.block.length) {
      found.start = found.index;
    } else {
      found.start = found.block.length - size;
    }
    return found;
  }

  function removeAssignment(side, got, delta) {
    var slot = got.block[got.start+delta];
    var old = assignment[side+slot];
    if (!old) return;
    var size = wallSize(old.wall);
    var from = got.block.indexOf(old.slot);

    $('#full-'+side+'-'+old.slot+'-'+old.wall).hide();
    for (var i=0; i<size; i++) {
      var next_slot = got.block[from+i];
      delete assignment[side+next_slot];
    }
  }

  function createAssignment(side, got, wall) {
    if (wall == 0) return;
    var slot = got.block[got.start];
    var size = wallSize(wall);

    $('#full-'+side+'-'+slot+'-'+wall).show();
    for (var i=0; i<size; i++) {
      var next_slot = got.block[got.start+i];
      assignment[side+next_slot] = {wall: wall, slot: slot, side: side};
    }
  }

  function setMiniLayers() {
    $('.generated-image > div').hide();
    $.each(assignment, function(_key, it){
      $('#mini-'+it.side+'-'+it.slot+'-'+it.wall).show();
    });
  }

  function setDescriptions() {
    var parts = modelParts();
    $('.generated-content > span[id^="desc_"]').empty();

    $.each(collectDesc('face', parts[0]), function(i, desc){
      $('#desc_a'+(i+1)).html(desc);
    });
    $.each(collectDesc('face', parts[1]), function(i, desc){
      $('#desc_b'+(i+1)).html(desc);
    });
    $.each(collectDesc('back', parts[0]), function(i, desc){
      $('#desc_c'+(i+1)).html(desc);
    });
    $.each(collectDesc('back', parts[1]), function(i, desc){
      $('#desc_d'+(i+1)).html(desc);
    });

    var gutters = $('input[name="gutters"]:checked').data('label');
    $('#desc_gutters').html(gutters);
  }

  function collectDesc(side, blocks) {
    var slots = [];
    slots = slots.concat.apply(slots, blocks); // multi-concat
    var list = [];
    $.each(slots, function(i, slot){
      var item = assignment[side+slot];
      if (!item || item.slot != slot) return;
      var size = wallSize(item.wall);
      var numb = ''+(i+1);
      if (size > 1) numb += ','+(i+2);
      if (size > 2) numb += ','+(i+3);
      list = list.concat(numb + '. ' + wallText(item.wall));
    });
    return list;
  }

  // TODO: run on slot select
  function findBlock(slot) {
    var parts = modelParts();
    var blocks = parts[0].concat(parts[1]);
    var found, index;
    $.each(blocks, function(_i, block){
      $.each(block, function(j, it){
        if (it == slot) {
          found = block;
          index = j;
          return false;
        }
      });
      if (found) return false;
    });
    return {block: found, index: index};
  }

  function modelParts() {
    return $('#work-container').data('parts');
  }

  function normalizeSlot(slot) {
    return slot_normal[slot] || slot;
  }

  // instead of a hash data
  function wallSize(wall) {
    return wall == 8 ? 3 : wall == 7 ? 2 : 1;
  }

  function wallText(wall) {
    return $('#wall-'+wall).data('label');
  }

  // --- wizard helpers ---

  function onLeaveStep(obj) {
    var step = obj.attr('rel');

    if (step == 1) {
      if (validateStep1()) {
        loadModel();
      } else {
        return false;
      }
    }

    return true;
  }

  function onShowStep(obj) {
    var step = obj.attr('rel');

    if (step == 4) {
      setMiniLayers();
      setDescriptions();
      makeSnapshot();
    }
  }

  function onFinish() {
    if (validateAllSteps()) {
      submitForm();
    }
  }

  function loadModel() {
    var $el = $('input:radio[name="model"]:checked');
    if (!$el.length) return;

    var model = $el.val();
    var model_id = model.substr(0,2);
    var $full = $('#model-builder > table > tbody > tr');
    var $mini = $('#step-4 > table > tbody > tr');

    $('#model_name').html($el.data('title'));
    $('#step4_model_name').html($el.data('title'));
    $('#model-builder').removeClass().addClass(model_id).show();
    $('#model-result').removeClass().addClass(model_id);

    $full.empty().load('includes/full_'+model+'.php', onLoadedModel);
    $mini.empty().load('includes/mini_'+model+'.php');

    assignment = {};
  }

  function onLoadedModel() {
    $('#side-select-face').click();
  }

  // --- step-4 helpers ---

  function makeSnapshot() {
    html2canvas($('#combo_one'), {
      onrendered: function(canvas){
        $('#image_one').val(canvas.toDataURL());
      }
    });
    html2canvas($('#combo_two'), {
      onrendered: function(canvas){
        $('#image_two').val(canvas.toDataURL());
      }
    });
  }

  function submitForm(preview) {
    if (!$('#image_one').val() || !$('#image_two').val()) return;
    setHiddenInputs();

    if (preview) {
      $('#wizard-form').attr('target', 'preview');
      $('#preview').val('1');
    } else {
      $('#wizard-form').removeAttr('target');
      $('#preview').val('');
    }

    $('#wizard-form').submit();
  }

  function setHiddenInputs() {
	var $el = $('input:radio[name="model"]:checked');
    $('#desc_side_a').val(buildDescSide('a'));
    $('#desc_side_b').val(buildDescSide('b'));
    $('#desc_side_c').val(buildDescSide('c'));
    $('#desc_side_d').val(buildDescSide('d'));
    $('#desc_gutter').val($('#desc_gutters').text());
    $('#desc_model_name').val($el.data('title'));
  }

  function buildDescSide(x) {
    return $('.generated-content > span[id^="desc_'+x+'"]').not(':empty').map(function(){
      return $(this).text();
    }).get().join('<br/>');
  }

  // --- wizard validation ---

  function validateAllSteps() {
    return validateStep1() && validateStep4();
  }

  function validateStep1() {
    var valid = true;
    // must select model
    if ($('input:radio[name="model"]').is(':checked')) {
      hideError(1);
    } else {
      valid = false;
      showError(1, T('Please select a model'));
    }
    return valid;
  }

  function validateStep4() {
    var valid = true;
    // must enter valid email
    var email = $('#email').val();
    if (!email) {
      valid = false;
      showError(4, T('Please enter e-mail address'));
    } else if (!isValidEmailAddress(email)) {
      valid = false;
      showError(4, T('E-mail address is invalid!'));
    } else {
      hideError(4);
    }
    return valid;
  }

  function isValidEmailAddress(email) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(email);
  }

  function showError(step, message) {
    $('#wizard').smartWizard('showMessage', message);
    $('#wizard').smartWizard('setError', { stepnum: step, iserror: true });
  }

  function hideError(step) {
    $('.msgBox').hide();
    $('#wizard').smartWizard('setError', { stepnum: step, iserror: false });
  }

  function T(str) {
    return _T[str] || str;
  }

// END module
