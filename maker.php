<?php

// DEBUG mode
ini_set('error_reporting', E_ALL ); // && !E_NOTICE
ini_set('display_errors', 1);

require_once("libs/dompdf/dompdf_config.inc.php");
require_once("libs/class.phpmailer-lite.php");
require_once("config.php");

function buildPDF($html) {
  $dompdf = new DOMPDF();
  $dompdf->load_html($html);
  $dompdf->set_paper('a4', 'portrait');
  $dompdf->render();
  return $dompdf;
}

function buildHTML($filename, $date, $oid, $provider) {
  ob_start();
  include($filename);
  return ob_get_clean();
}

function buildEmail($filename, $vars) {
  ob_start();
  include($filename);
  return ob_get_clean();
}

function sendEmail($to, $subject, $body, $attachment, $config) {
  $mail = new PHPMailerLite();
  $mail->IsMail();
  $mail->CharSet = 'utf-8';
  $mail->AddAddress($to);
  $mail->SetFrom($config['from'], $config['from_name']);
  $mail->Subject = $subject;
  $mail->MsgHTML($body);
  $mail->AddStringAttachment($attachment, $config['file_name']);
  $ok = $mail->Send();
  if ($ok) {
    return '';
  } else {
    return 'Mailer Error: '.$mail->ErrorInfo;
  }
}

function completeOrder($ordernr, $date, $pdf, $config, $provider) {
  $client_email = $_POST['email'];
  $orders_email = $_POST['email']; // <<< TEMPORARY, $config['send_to'];

  $subject = 'Užsakymas Nr. '.$ordernr.'. Data '.$date;
  $vars = array(
    'ordernr' => $ordernr,
    'phone'   => $provider['phone'],
    'mobile'  => $provider['mobile'],
    'email'   => $provider['email'],
  );
  $attachment = $pdf->output();

  $body = buildEmail('templates/order.php', $vars);
  $rsp1 = sendEmail($orders_email, $subject, $body, $attachment, $config);
  $body = buildEmail('templates/client.php', $vars);
  $rsp2 = sendEmail($client_email, $subject, $body, $attachment, $config);

  if (empty($rsp1) && empty($rsp2)) {
    header('Location: index.php#done');
  } else {
    echo 'ERROR (mailer): '.$rsp1.$rsp2; // TODO: proper error handling
  }
}

  // ------ MAIN ------

  $date = date('Y m d');
  $oid  = date('His'); // <<< TEMPORARY, what?
  $ordernr = date('Ymd').$oid;
  $config = getConfig();
  $provider = getProvider();

  $html = buildHTML('templates/pdf.php', $date, $oid, $provider);
  $pdf = buildPDF($html);

  if (!empty($_POST['preview'])) {
    $pdf->stream('namukas.pdf', array('Attachment' => 0));
  } else {
    completeOrder($ordernr, $date, $pdf, $config, $provider);
  }
?>