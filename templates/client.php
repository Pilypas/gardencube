Sveiki, jūs užsakėte gaminį Nr. <?= $vars['ordernr'] ?>. Detali užsakymo informacija pateikta prisegtame PDF dokumente.<br/>
Jūsų užsakymas išsiųstas gamintojui, kuris per 2 darbo dienas su jumis susisieks ir pateiks kainos pasiūlymą kartu su kitomis pirkimo sąlygomis.
<br/><br/>

Ši žinutė yra automatinė, prašau į ją neatsakyti.
<br/><br/>

Dėl informacijos teiraukitės:<br/>
Tel. <?= $vars['phone'] ?><br/>
Mob. tel. <?= $vars['mobile'] ?><br/>
E-mail: <?= $vars['email'] ?><br/>
