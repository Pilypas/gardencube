Sveiki, klientas užsakė gaminį Nr. <?= $vars['ordernr'] ?>. Detali užsakymo informacija pateikta prisegtame PDF dokumente.<br/>
Šis užsakymas išsiųstas klientui, kuriam per 2 darbo dienas turite pateikti kainos pasiulymą kartu su kitomis pirkimo salygomis.
<br/><br/>

Informacija apie klientą:
<br/><br/>

Vardas: <?= $_POST['name'] ?><br/>
Pavarde: <?= $_POST['surname'] ?><br/>
El. pašto adresas: <?= $_POST['email'] ?><br/>
Telefono Nr.: <?= $_POST['phone'] ?><br/>
Adresas: <?= $_POST['address'] ?><br/>
<? if (!empty($_POST['note'])): ?>
Pastabos: <?= $_POST['note'] ?>
<? endif ?>
<br/><br/>

Ši žinutė yra automatinė, prašau į ją neatsakyti.
