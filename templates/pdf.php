<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <style type="text/css">
    * { font-family: "Arial"; }
    p { font-size: 12px; }
    table { width:100%; }
    div.logo { position: absolute; top: 0px; left: 0px; font-size: 36px; font-weight: bold; }
    p.top { font-size: 19px; text-align: center; margin-left: 180px; }
    p.head .big { font-size: 24px; }
    table.main { margin-bottom: 10px; }
    table.main td { font-size: 13px; vertical-align: top; }
    img.one { width:340px; height:260px; }
    img.two { width:340px; height:260px; }
    table.bottom { margin-top: 80px; }
    table.bottom td { font-size: 12px; vertical-align: top; }
    table.bottom td.half { width: 50%; }
  </style>
</head>
<body>
  <div class="logo"><img src="images/tema/logo.jpg" /></div>
  <p class="top">
    Susikonstruokite pavėsinę pagal savo skonį!
  </p>
  <hr/>
  <p class="head">
    <b class="big">GAMINIO PAVADINIMAS</b><br/>
    <b>Užklausos Nr.</b> <?= $oid ?><br/>
    <b>Data:</b> <?= $date ?>
  </p>
  <table class="main">
    <tr>
      <td>
        <b>Pavėsinės A pusės sienos:</b><br/>
        <?= $_POST['desc_side_a'] ?>
      </td>
      <td>
        <b>Pavėsinės C pusės sienos:</b><br/>
        <?= $_POST['desc_side_c'] ?>
      </td>
    </tr>
    <tr>
      <td>
        <b>Pavėsinės B pusės sienos:</b><br/>
        <?= $_POST['desc_side_b'] ?>
      </td>
      <td>
        <b>Pavėsinės D pusės sienos:</b><br/>
        <?= $_POST['desc_side_d'] ?>
      </td>
    </tr>
    <tr>
      <td>
        <b>Lietvamzdžiai:</b> <?= $_POST['desc_gutter'] ?>
      </td>
    </tr>
  </table>
  <table class="images">
    <tr>
      <td>
        <img class="one" src="<?= $_POST['image_one'] ?>"/>
      </td>
      <td>
        <img class="two" src="<?= $_POST['image_two'] ?>"/>
      </td>
    </tr>
  </table>
  <table class="bottom">
    <tr>
      <td class="half">
        Jūsų kontaktiniai duomenys
        <hr/>
        <table class="user">
          <tr>
            <td>Vardas:</td>
            <td><?= $_POST['name'] ?></td>
          </tr>
          <tr>
            <td>Pavardė:</td>
            <td><?= $_POST['surname'] ?></td>
          </tr>
          <tr>
            <td>El. paštas:</td>
            <td><?= $_POST['email'] ?></td>
          </tr>
          <tr>
            <td>Tel.</td>
            <td><?= $_POST['phone'] ?></td>
          </tr>
          <tr>
            <td>Adresas:</td>
            <td><?= $_POST['address'] ?></td>
          </tr>
          <?php if (!empty($_POST['note'])): ?>
          <tr>
            <td>Pastabos:</td>
            <td><?= $_POST['note'] ?></td>
          </tr>
          <?php endif ?>
        </table>
      </td>
      <td class="half">
        Mūsų kontaktiniai duomenys
        <hr/>
        <b><?= $provider['name'] ?></b><br/>
        <?= $provider['code'] ?><br/>
        PVM k. <?= $provider['vat'] ?><br/><br/>

        <?= $provider['address1'] ?><br/>
        <?= $provider['address2'] ?><br/><br/>

        Tel. <?= $provider['phone'] ?><br/>
        Mob. tel. <?= $provider['mobile'] ?><br/><br/>

        El. p. <?= $provider['email'] ?><br/>
        <?= $provider['website'] ?>
      </td>
    </tr>
  </table>
</body>
</html>